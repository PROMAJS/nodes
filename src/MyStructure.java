import java.util.List;
import java.util.Optional;

public class MyStructure implements IMyStructure {

    private List<INode> nodes;

    @Override
    public INode findByCode(String code) {

        Optional<INode> iNodeOptional = nodes.stream()
                .filter(e -> e.getCode().equals(code))
                .findAny();

        if (iNodeOptional.isPresent()) {
            return iNodeOptional.get();
        }
        return null;
    }

    @Override
    public INode findByRedender(String renderer) {
        Optional<INode> iNodeOptional = nodes.stream()
                .filter(e -> e.getRenderer().equals(renderer))
                .findAny();

        if(iNodeOptional.isPresent()){
            return iNodeOptional.get();
        }
        return null;
    }

    @Override
    public int count() {
        return nodes.size();
    }

    /*

    public void printNodes(List<INode> nodes, ICompositeNode iCompositeNode)
    {
        for(INode node : nodes){
            iCompositeNode.getNodes(node);
        }
    }

    */


}
