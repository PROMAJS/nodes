public interface IMyStructure {

    INode findByCode(String code);

    INode findByRedender(String renderer);

    int count();

}
